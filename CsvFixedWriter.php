<?php

/**
 * CsvFixedWriter
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2018-2024 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 0.3.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Afferor General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class CsvFixedWriter extends Writer
{
    /** The options **/
    /* @var string */
    public $delimiter = ",";
    /* @var string */
    public $enclosure = '"';
    /* @var string */
    public $escapechar = "\\";
    /* @var boolean */
    public $striptag;
    /* @var boolean|string */
    public $stripnewline = false;
    /* @var boolean */
    public $addnewline = false;
    /* @var string */
    public $fixutf8 = '';

    /* @var null|string */
    private $output;
    /* @var boolean */
    private $hasOutputHeader = false;

    /**
     * Output file, can be a file or php://output
     * @var string (with scheme)
     **/
    private $file = null;

    /**
     * The filename to use for the resulting file when output = display
     * @var string
     */
    protected $csvFilename = '';

    /**
     * Should headers be output? For example spss and r export use more or less
     * the same output but do not need headers at all.
     *
     * @var boolean
     */
    protected $doHeaders = true;

    function __construct()
    {
        $this->output = '';
        $this->hasOutputHeader = false;
    }

    public function init(SurveyObj $survey, $sLanguageCode, FormattingOptions $oOptions)
    {
        parent::init($survey, $sLanguageCode, $oOptions);
        $this->csvFilename = "results-survey" . $survey->id . ".csv";

        if ($oOptions->output == 'file') {
            $this->output = 'file';
            $this->file = fopen($this->filename, 'w');
        } else {
            header("Content-Disposition: attachment; filename=" . $this->csvFilename);
            header("Content-type: text/comma-separated-values; charset=UTF-8");
            $this->file = fopen('php://output', 'w');
        }
    }

    protected function outputRecord($headers, $values, FormattingOptions $oOptions)
    {
        $sRecord = '';
        if (!$this->hasOutputHeader) {
            fwrite($this->file, chr(239) . chr(187) . chr(191)); // Write UTF-8 Byte Order Mark (BOM) for … MS
            // If we don't want headers in our csv, for example in exports like r/spss etc. we suppress the header by setting this switch in the init
            if ($this->doHeaders === true) {
                $index = 0;
                foreach ($headers as $header) {
                    $headers[$index] = $this->updateValues($header, true);
                    $index++;
                }
                //Output the header...once and only once.
                fputcsv($this->file, $headers, $this->delimiter, $this->enclosure, $this->escapechar);
            }
            $this->hasOutputHeader = true;
        }
        //Output the values.
        $index = 0;
        foreach ($values as $value) {
            $values[$index] = $this->updateValues($value);
            $index++;
        }
        fputcsv($this->file, $values, $this->delimiter, $this->enclosure, $this->escapechar);
    }

    public function close()
    {
        // Output white line at the end, better for R import
        if ($this->addnewline) {
            fwrite($this->file, "\r\n"); // Seemls to be for R and spss
        }
        fclose($this->file);
    }

    /**
     * Returns the value with all necessary escaping needed to place it into a CSV string.
     *
     * @param string $value
     * @return string
     */
    protected function updateValues($string, $header = false)
    {
        $string = preg_replace(array('~\R~u'), array("\n"), $string);
        $stripnewline = (($header && $this->stripnewline == "header") || $this->stripnewline == "always");
        if ($this->striptag || $stripnewline) {
            $string = str_replace("\n", " ", $string);
        }
        if ($this->striptag) {
            $string = preg_replace('@<script[^>]*?>.*?</script>@si', '', $string);
            $string = strip_tags($string);
        }
        if ($this->fixutf8) {
            /* @see https://stackoverflow.com/a/13695364/2239406 */
            switch ($this->fixutf8) {
                case 'mbconvertencoding':
                    /* leave mb_substitute_character as is */
                    $string = mb_convert_encoding($string, 'UTF-8', 'UTF-8');
                    break;
                case 'htmlspecialchars':
                    $string = htmlspecialchars_decode(htmlspecialchars($string, ENT_SUBSTITUTE, 'UTF-8'));
                    break;
                default:
                    // invalid : no action
            }
        }
        return $string;
    }
}
