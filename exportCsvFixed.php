<?php

/**
 * exportCsvConfigurable
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2018-2024 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 0.3.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Afferor General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class exportCsvFixed extends PluginBase
{
    protected $storage = 'DbStorage';
    protected static $name = 'exportCsvFixed';
    protected static $description = 'Allow to configure some csv options globally.';

    protected $settings = array(
        'replace' => array(
            'type' => 'boolean',
            'label' => 'Replace exiting csv',
            'default' => 1,
        ),
        'asdefault' => array(
            'type' => 'boolean',
            'label' => 'Set to default (if not replace)',
            'default' => 1,
        ),
        'delimiter' => array(
            'type' => 'select',
            'options' => array(
                ',' => "Comma (real csv)",
                ';' => "Semicolon",
                "\t" => "Tabulation",
            ),
            'label' => 'Separator',
            'default' => ",",
        ),
        'enclosure' => array(
            'type' => 'select',
            'options' => array(
                '"' => "Double quote",
                '\'' => "Simple quote",
            ),
            'label' => 'Enclosure',
            'default' => '"',
        ),
        'striptag' => array(
            'type' => 'boolean',
            'label' => 'Strip tag and javascript',
            'default' => 0,
            'help' => "This strip new line",
        ),
        'stripnewline' => array(
            'type' => 'select',
            'options' => array(
                //'survey' => 'On survey part, not user data', // Need to check question type … @todo
                'header' => "On header, not on data",
                'always' => "Yes (always)",
            ),
            'htmlOptions' => array(
                'empty' => "No"
            ),
            'label' => 'Strip new line',
            'default' => '',
        ),
        'addnewline' => array(
            'type' => 'boolean',
            'label' => 'Add a new line at end of CSV file',
            'default' => 0,
        ),
        'fixutf8' => array(
            'type' => 'select',
            'label' => 'Remove invalid caracter',
            'options' => [
                'mbconvertencoding' => 'Using mb_convert_encoding',
                'htmlspecialchars' => 'Using htmlspecialchars',
            ],
            'htmlOptions' => array(
                'empty' => "No"
            ),
            'default' => '',
        ),
    );

    /**
     * @inheritdoc
     * Remove settings according to version
     */
    public function getPluginSettings($getValues = true)
    {
        $pluginSettings = parent::getPluginSettings($getValues);
        if (intval(App()->getConfig('versionnumber') >= 5)) {
            unset($pluginSettings['delimiter']);
        }
        return $pluginSettings;
    }
    /** @inheritdoc */
    public function init()
    {
        $this->subscribe('listExportPlugins');
        $this->subscribe('listExportOptions');
        $this->subscribe('newExport');
    }

    public function listExportOptions()
    {
        $event = $this->getEvent();
        $type = $event->get('type');
        $label = $this->gT("Fixed csv");
        if ($this->get('replace', null, null, $this->settings['replace']['default'])) {
            $label = $this->gT("CSV");
        }
        switch ($type) {
            case 'csv-fixed':
                $event->set('label', $label);
                if ($this->get('replace', null, null, $this->settings['replace']['default']) || $this->get('asdefault', null, null, $this->settings['asdefault']['default'])) {
                    $event->set('default', true);
                }
            default:
                break;
        }
    }

    /**
    * Registers this export type
    */
    public function listExportPlugins()
    {
        $event = $this->getEvent();
        $exports = $event->get('exportplugins');
        $newExport = array('csv' => $exports['csv'],'csv-fixed' => get_class());
        if ($this->get('replace', null, null, $this->settings['replace']['default'])) {
            $newExport = array('csv-fixed' => get_class());
        }
        unset($exports['csv']);
        $exports = $newExport + $exports;
        $event->set('exportplugins', $exports);
    }
    public function newExport()
    {
        $event = $this->getEvent();
        $type = $event->get('type');
        switch ($type) {
            case 'csv-fixed':
                $writer = new CsvFixedWriter();
                if (intval(App()->getConfig('versionnumber') >= 5)) {
                    $writer->delimiter = App()->getRequest()->getPost('csvfieldseparator', ',');
                } else {
                    $writer->delimiter = $this->get('delimiter', null, null, $this->settings['delimiter']['default']);
                }
                $writer->enclosure = $this->get('enclosure', null, null, $this->settings['enclosure']['default']);
                $writer->striptag = $this->get('striptag', null, null, $this->settings['striptag']['default']);
                $writer->stripnewline = $this->get('stripnewline', null, null, $this->settings['stripnewline']['default']);
                $writer->addnewline = $this->get('addnewline', null, null, $this->settings['addnewline']['default']);
                $writer->fixutf8 = $this->get('fixutf8', null, null, $this->settings['fixutf8']['default']);
                $event->set('writer', $writer);
                break;
            default:
        }
    }
}
